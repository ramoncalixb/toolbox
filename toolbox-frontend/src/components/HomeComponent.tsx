import React, {useState} from 'react'
import ReactDOM from 'react-dom';

import { Card, Form, Button, Spinner } from 'react-bootstrap'
import Editor from './EditorComponent'
import axios from 'axios'
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';

const Home: React.FC = () => {
    const [data, setData] = useState() 
    const [loading, setLoading] = useState(false) 
    const [editorData, setEditorData] = useState() 
    const [text, setText] = useState();

    const createMarkup = (text) => {
        const newHtml = draftToHtml(text);
        return {__html: newHtml};
    }

    const modifyText = (text) => {
        setText(text) 
    }

    const sendData = async () => {
        setLoading(true)
            try {
                const saveData = await axios.post('http://localhost:3001/api/content/message', {data: convertToRaw(text.getCurrentContent())})
                if ('data' in saveData) {
                    ReactDOM.render(<div dangerouslySetInnerHTML={createMarkup(saveData.data)}/>, document.getElementById('newRender'));
                    setLoading(false)
                }
            } catch (error) {
                console.error(error);
                }
            }
    return (
        <div>
            <Card>
                <Card.Header as="h5">Toolbox test </Card.Header>
                <Card.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Text Content </Form.Label>
                        <Editor modifyText={modifyText}/>
                        <Form.Text className="text-muted"  >
                        Plese test all options 
                        </Form.Text>
                    </Form.Group>
                        <Button variant="primary" size="lg" id="save" block onClick={sendData}>
                            Save
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
            <Card>
                <Card.Header as="h5">Content Result from api </Card.Header>
                <Card.Body>
                     <div id="newRender"></div>
                    {loading &&  <Button variant="primary" disabled>
                        <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            />
                        Loading...
                    </Button>}
                </Card.Body>
            </Card>
        </div>
    )
}

export default Home;