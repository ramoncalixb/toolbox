
import { logger } from '@shared';
import { Request, Response, Router, Express } from 'express';
import { BAD_REQUEST, CREATED, OK } from 'http-status-codes';
import { paramMissingError } from '@shared';
import { ParamsDictionary } from 'express-serve-static-core';

// Init shared
const router = Router();

router.post('/message', async (req: Request, res: Response) => {
    try {
        const { data } = req.body;
        if (!data) {
            return res.status(BAD_REQUEST).json({
                error: paramMissingError,
            });
        }
        return res.status(CREATED).json(data);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(BAD_REQUEST).json({
            error: err.message,
        });
    }
});


export default router;
