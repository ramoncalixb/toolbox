import app from '@server';
import supertest from 'supertest';

import { BAD_REQUEST, CREATED, OK } from 'http-status-codes';
import { Response, SuperTest, Test, Request } from 'supertest';
import { pErr, paramMissingError } from '@shared';

describe('Message Routes', () => {
  const messagePath = '/api/content/message';
  let agent: SuperTest<Test>;

  beforeAll((done) => {
    agent = supertest.agent(app);
    done();
  });

  describe(`"Post:${messagePath}"`, () => {
    it(`Should be render a random content  a status code of "${OK}" if the
    request was successful.`, (done) => {
      const callApi = (reqBody: object) => {
        return agent.post(messagePath).type('form').send(reqBody);
      };

      callApi({ data: 'new Message' })
        .end((err: Error, res: Response) => {
          pErr(err);
          expect(res.status).toBe(CREATED);
          expect(res.body).toBe('new Message');
          done();
        });
    })

    it(`Should be respond with a structure `, (done) => {
      const callApi = (reqBody: object) => {
        return agent.post(messagePath).type('form').send(reqBody);
      };
      callApi({ data: { contet: 'text' } })
        .end((err: Error, res: Response) => {
          pErr(err);
          expect(res.status).toBe(CREATED);
          expect(res.body).toEqual({ contet: 'text' });
          done();
        });
    });

    it(`Should be respond with a error`, (done) => {
      const callApi = (reqBody: object) => {
        return agent.post(messagePath).type('form').send(reqBody);
      };
      callApi({})
        .end((err: Error, res: Response) => {
          pErr(err);
          expect(res.status).toBe(BAD_REQUEST);
          done();
        });
    });
  });

});
