import { Router } from 'express';
import MessageRouter from './Message';

// Init router and path
const router = Router();

// Add sub-routes
router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
router.use('/content', MessageRouter);

// Export the base-router
export default router;
