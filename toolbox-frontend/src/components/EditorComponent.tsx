import React, { useState } from 'react';


import { Editor } from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

interface CustomProps {
  modifyText: any
}
const ControlledEditor: React.SFC<CustomProps> = (props: CustomProps) => {
    return (
      <div>
      <Editor
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        onEditorStateChange={props.modifyText}
      />
      </div>
    )
  }

export default ControlledEditor;