Setup both projects 

Steps to install : 
- git clone https://gitlab.com/ramoncalixb/toolbox.git
- cd backend-toolbox
- npm install
- cd toolbox-frontend
- npm install

For run:
- cd backend-toolbox
-  npm run  start-dev
-  cd toolbox-frontend
-  yarn start

For Test:
- cd backend-toolbox
- npm run test
- cd toolbox-frontend
- npm test