import React from 'react';
import Home from './HomeComponent';
import renderer, { act }  from 'react-test-renderer';
import axios from "axios";
const content = {"entityMap":{},"blocks":[{"key":"637gr","text":"Initialized from content state.","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}]};

test('Home Page should be render', () => {
    const getSpy = jest.spyOn(axios, 'get');
    const component = renderer.create(
        <Home/>,
    );
    let HomeComponent = component.toJSON();
    expect(HomeComponent).toMatchSnapshot();

    act(() => {
        component.root.findByProps({ 'id': 'save' }).props.onClick(); 
        expect(getSpy).toBeCalled();
    })
});